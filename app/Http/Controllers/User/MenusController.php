<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MenusController extends Controller
{
      public function sobre()
    {
      return view ('user.sobre');
    }

     public function vantagens()
    {
      return view ('user.vantagens');
    }
    
    public function subscricao()
    {
        return view ('user.subscricao');
    }
    
    public function resultados() 
    {
        return view ('user.resultados');
    }
    
}
