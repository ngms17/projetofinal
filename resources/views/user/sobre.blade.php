@extends('user/app')

@section('bg-img', asset('user/img/fundo.jpeg'))

@section('title', 'Sobre Nós')


@section('main-content')

<div class="jumbotron jumbotron-fluid">
  <div class="container">
    <h1 class="display-4">Sobre Nós</h1>
    <p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur ut tortor quis sapien mattis accumsan. Cras accumsan imperdiet risus non egestas. Phasellus ut mollis metus, pretium laoreet justo. Duis accumsan varius sollicitudin. Morbi rutrum urna risus, eget imperdiet enim volutpat eget. Curabitur eu mattis orci, ac consectetur ipsum. Duis ultrices ut tellus ut pharetra. Donec feugiat, orci et finibus blandit, urna felis bibendum urna, eu sodales tortor libero eu erat. Vestibulum fringilla consectetur sagittis.

Suspendisse non finibus nibh. Duis condimentum tempor nisi quis aliquam. Ut accumsan odio tincidunt ultricies accumsan. Morbi aliquet felis sed nisi elementum, non gravida leo mattis. In tincidunt, justo eu sodales tristique, nunc dolor fermentum mi, at congue enim neque vitae ante. Nunc pretium fringilla sapien, sit amet ornare mi molestie vel. Pellentesque varius erat vel arcu volutpat, vitae tempor est sodales.</p>
  </div>
</div>


@endsection
