@extends('user/app')

@section('bg-img', asset('user/img/fundo.jpeg'))

@section('title', 'Resultados do Serviço')


@section('main-content')

<center>
<div class="container">
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-auto" align="center" src="https://i.imgur.com/0PkDvn7.png" alt="First slide">
        <h5>Resultados Janeiro</h5>
    <p>Resumo dos resultados</p>
    </div>
    <div class="carousel-item">
      <img class="d-block w-auto" align="center" src="https://i.imgur.com/0PkDvn7.png" alt="Second slide">
        <h5>Resultados Fevereiro</h5>
    <p>Resumo dos resultados</p>
    </div>
    <div class="carousel-item">
      <img class="d-block w-auto" align="center" src="https://i.imgur.com/0PkDvn7.png" alt="Third slide">
        <h5>Resultados Março</h5>
    <p>Resumo dos resultados</p>
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
</div>
    </center>


@endsection