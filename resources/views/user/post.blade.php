@extends('user/app')
@section('bg-img', Storage::disk('local')->url($post->image))
@section('title', $post->title)
@section('sub-heading', $post->subtitle)
@section('main-content')
<!-- Post Content -->

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/pt_PT/sdk.js#xfbml=1&autoLogAppEvents=1&version=v3.0&appId=406444006432365';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>


<article>
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
        <small>Postado em {{$post->created_at}}</small>

          @foreach($post->categories as $category)
              <small class="pull-right" style="margin-right: 20px">
                <a href="{{ route('category', $category->slug) }}">{{ $category->name }}</a>
              </small>
          @endforeach

        {!! htmlspecialchars_decode($post->body) !!}

        <h3>Tags</h3>
          @foreach($post->tags as $tag)
            <a href="{{ route('tag', $tag->slug) }}"><small class="pull-left" style="margin-right: 20px;border-radius: 5px;border: 1px solid gray;padding: 5px">
              {{ $tag->name }}
            </small></a>
            @endforeach
      </div>

      <div class="fb-comments" data-href="{{ Request::url() }}" data-numposts="5"></div>

    </div>
  </div>
</article>

<hr>
@endsection
