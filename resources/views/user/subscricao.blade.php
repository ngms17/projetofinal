@extends('user/app')

@section('bg-img', asset('user/img/fundo.jpeg'))

@section('title', 'Subscrição')


@section('main-content')

    <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
      <h1 class="display-4">Planos de Subscrição</h1>
      <p class="lead">Para ter acesso aos nossos prognósticos Premium terá de efetuar a compra desta subscrição.</p>
    </div>

    <div class="container">
      <div class="card-deck mb-3 text-center">
        <div class="card mb-4 box-shadow">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal">Registado</h4>
          </div>
          <div class="card-body">
            <h1 class="card-title pricing-card-title">0€ <small class="text-muted">/ mês</small></h1>
            <ul class="list-unstyled mt-3 mb-4">
              <li>Acesso às Tips Gratuitas</li>
            </ul>
<!--            <button type="button" class="btn btn-lg btn-block btn-outline-primary" href="http://google.pt">Registar</button>-->
              <a href="/register" class="btn btn-lg btn-block btn-outline-primary" role="button" aria-pressed="false">Registar</a>
          </div>
        </div>
        <div class="card mb-4 box-shadow">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal">Premium</h4>
          </div>
          <div class="card-body">
            <h1 class="card-title pricing-card-title">15€ <small class="text-muted">/ mês</small></h1>
            <ul class="list-unstyled mt-3 mb-4">
              <li>Acesso às Tips Premium e todas as suas vantagens</li>
            </ul>
<!--            <button type="button" class="btn btn-lg btn-block btn-primary">Subscrever</button>-->
            <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">
            <input type="hidden" name="cmd" value="_s-xclick">
            <input type="hidden" name="hosted_button_id" value="6RNT8A4HBBJRE">
        <input type="image" src="https://www.paypalobjects.com/pt_PT/PT/i/btn/btn_subscribeCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
              <img alt="" src="https://www.sandbox.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
            </form>
            </div>
        </div>
            <a href="/contact" class="btn btn-lg btn-block btn-primary" role="button" aria-pressed="false">Contacte-nos</a>
          </div>
        </div>



@endsection