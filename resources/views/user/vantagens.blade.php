@extends('user/app')

@section('bg-img', asset('user/img/fundo.jpeg'))

@section('title', 'Vantagens')


@section('main-content')

<div class="container" align="center">
<div class="card" style="width: 25rem;">
  <div class="card-header">
    Vantagens do nosso serviço Premium 🎖
  </div>
  <ul class="list-group list-group-flush">
    <li class="list-group-item">Acesso às tips com maior probabilidade de acerto</li>
    <li class="list-group-item">Gestão de Banca</li>
    <li class="list-group-item">Acompanhamento através de chat</li>
      <li class="list-group-item">Planilhas Excel para registo diário de entradas efetuadas</li>
  </ul>
    <a href="/subscricao" class="btn btn-primary">Subscrever Serviço</a>
</div>
</div>


@endsection
