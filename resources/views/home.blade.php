@extends('user/app')
@section('bg-img', asset('user/img/fundo.jpeg'))
@section('title', 'BetProject')
@section('sub-heading')
@section('main-content')


<article>
  <div class="container">
    <div class="jumbotron">
  <h1 class="display-4">Bem-vindo à betProject!</h1>
  <p class="lead">O local certo para obter o maior lucro nas apostas desportivas</p>
  <hr class="my-4">
  <p>Registe-se para ter acesso aos prognósticos gratuitos ou escolha um plano de subscrição.</p>
  <a class="btn btn-primary btn-lg" href="{{ route('vantagens') }}" role="button">Saiba mais</a>
</div>
    <div class="card">
  <div class="card-body">
    <script src="http://webmasters.onlinebettingacademy.com/assets/js/webmasters.js" data-title="Jogos e Resultados do Dia" data-lang="pt" data-timezone="Europe/Lisbon" data-height="600" data-widget="livescores" data-color="3b5998|8b9dc3|dfe3ee|f7f7f7|ffffff|333333|5f7ec1|263961|d0d0d0|7b7b7b" type="text/javascript"></script>
  </div>
</div>
  </div>
</article>

<hr>
@endsection
@section('footer')
@endsection
