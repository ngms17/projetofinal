@extends('user/app')

@section('bg-img', asset('user/img/fundo.jpeg'))

@section('title', 'Contacto')


@section('main-content')

<div class="container">
	
	@if(session('message'))
	<div class='alert alert-success'>
		{{ session('message') }}
	</div>
	@endif
	
	<div class="col-12 col-md-6">
		<form class="form-horizontal" method="POST" action="/contact">
			{{ csrf_field() }} 
			<div class="form-group">
			<label for="Name">Nome </label>
			<input type="text" class="form-control" id="name" placeholder="O seu nome" name="name" required>
		</div>

		<div class="form-group">
			<label for="email">Email: </label>
			<input type="text" class="form-control" id="email" placeholder="betproject@gmail.com" name="email" required>
		</div>

		<div class="form-group">
			<label for="message">Mensagem: </label>
			<textarea type="text" class="form-control luna-message" id="message" placeholder="Escreva aqui a sua mensagem..." name="message" required></textarea>
		</div>

			<div class="form-group">
				<button type="submit" class="btn btn-primary" value="Send">Enviar</button>
			</div>
		</form>
	</div>
 </div> <!-- /container -->



@endsection