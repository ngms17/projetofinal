<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/contact', 'ContactController@show');
Route::post('/contact',  'ContactController@mailToAdmin'); 

Route::get('/', 'HomeController@index')->name('home');

// User Routes
Route::group(['namespace' => 'User'], function(){

  Route::get('home', 'HomeController@index')->name('inicio');

  Route::get('sobre', 'MenusController@sobre')->name('sobre');

  Route::get('vantagens', 'MenusController@vantagens')->name('vantagens');
    
  Route::get('subscricao', 'MenusController@subscricao')->name('subscricao');
    
  Route::get('resultados', 'MenusController@resultados')->name('resultados');

  Route::get('post/{post}', 'PostController@post')->name('post');

  Route::get('post/tag/{tag}', 'HomeController@tag')->name('tag');

  Route::get('post/category/{category}', 'HomeController@category')->name('category');

});


Route::group(['namespace' => 'Admin'], function(){

  Route::get('admin/home', 'HomeController@index')->name('admin.home');

  // Users Routes

  Route::resource('admin/user', 'UserController');

  // Post Routes

  Route::resource('admin/post', 'PostController');

  // Tag Routes

  Route::resource('admin/tag', 'TagController');

  // Category Routes

  Route::resource('admin/category', 'CategoryController');

  // Admin Auth Route

  Route::get('admin-login', 'Auth\LoginController@showLoginForm')->name('admin.login');

  Route::post('admin-login', 'Auth\LoginController@Login');


});



  Auth::routes();

  // Route::get('/home', 'HomeController@index')->name('home');
